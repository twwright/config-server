/*
 * Model representing a profile for a particular application configuration
 */

var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var ProfileSchema = new Schema({
    name: { type: String, required: true},
    id: { type: String, required: true },
    //applicationId: { type: Schema.Types.ObjectId, required: true},
    created: { type: Date, required: true },
    modified: { type: Date, required: true },
    config: { type: {}, required: true}
}, { collection: 'profiles'});

// unique username validation
ProfileSchema.path('id').validate(function(value, done) {
    this.constructor.findOne({
        _id: { $ne: this._id},
        id: this.id
    }, function (err, results) {
        if (err)
            throw err;
        done(!results);
    });
}, 'Profile ID must be unique');


module.exports = mongoose.model('Profile', ProfileSchema);