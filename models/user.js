/**
 * Created by Tom on 23/08/2015.
 */

var mongoose = require('mongoose');

var Schema = mongoose.Schema;

// define the user schema
var UserSchema = new Schema({
    username: { type: String, required: true },
    password: { type: String, required: true },
    salt: { type: String, required: true }
}, { collection: 'users'});

// unique username validation
UserSchema.path('username').validate(function(value, done) {
    this.constructor.findOne({
        _id: { $ne: this._id},
        username: this.username
    }, function (err, results) {
        if (err)
            throw err;
        done(!results);
    });
}, 'Username must be unique, username already exists');

module.exports = mongoose.model('user', UserSchema);