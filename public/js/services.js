var profileServices = angular.module('profileServices', ['ngResource']);

profileServices.factory('Profile', ['$resource', function($resource) {
    return $resource('/api/profile/:id', { id: '@id' }, {
        query:  { method: 'GET',    params: { id: ''},     isArray: true}, // retrieve all
        get:    { method: 'GET' }, // retrieve one
        delete: { method: 'DELETE'}, // delete a profile
        insert: { method: 'POST',   params: { id: ''}}, // insert new
        save:   { method: 'PUT' } // update
    }, { stripTrailingSlashes: false });
}]);

profileServices.factory('OpenProfiles', function() {
    var service = {
        openProfiles: [],
        current: {}
    };
    service.add = function(profile) {
        var open = false;
        $.each(service.openProfiles, function(index, value) {
            if(value.id == profile.id)
                open = true;
        });
        if(!open)
            service.openProfiles.push(profile);
    };
    service.remove = function(profile) {
        console.log('remove');
        $.each(service.openProfiles, function(index, value) {
            if(value.id == profile.id)
                service.openProfiles.splice(index, 1);
        });
    };
    service.get = function() {
        return service.openProfiles;
    };
    service.setCurrent = function(id) {
        $.each(service.openProfiles, function(index, value) {
            if (value.id == id)
            {
                service.current = value;
            }
        });
    };

    return service;
});