var profileControllers = angular.module('profileControllers', []);

profileControllers.controller('ProfileListCtrl', ['$scope', 'Profile', 'OpenProfiles', function($scope, Profile, OpenProfiles) {
    $scope.token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1NWQ5N2QyOGI4ZTE2YWIwMjQyYmZjY2YiLCJ1c2VybmFtZSI6ImFwcCIsInBhc3N3b3JkIjoiJDJhJDEwJDZYendFL25hdHlQRGRqUnVXSk9nck9ERTJoOE5EdGhxeDVFcjlMdTIzbHFwdDNndkl1OXBHIiwic2FsdCI6IiQyYSQxMCQ2WHp3RS9uYXR5UERkalJ1V0pPZ3JPIiwiX192IjowfQ.zAHMFrB6wGkSclz6oBIZWWvFFN-9vZbJc6OEOFurQIg';
    $scope.loadProfiles = function() {
        // load profiles
        $scope.profiles = Profile.query({token: $scope.token});
    };
    $scope.loadProfiles();
    $scope.openProfiles = function() {
        $.each($scope.profiles, function(index, value) {
            if (value.selected) {
                OpenProfiles.add(value);
            }
        });
        $scope.deselect();
    };
    $scope.deleteSelected = function() {
        $.each($scope.profiles, function(index, value) {
            if(value.selected)
            {
                var profile = new Profile(value);
                profile.$delete({ token: $scope.token },
                    function(deleted, responseHeaders) {
                        // perform iteration again because indices may have changed due to other deletions!
                        $.each($scope.profiles, function(index, value) {
                            if(typeof(value) !== 'undefined' && value.id == profile.id)
                                $scope.profiles.splice(index, 1);
                        });
                        $.notify("Profile " + profile.id + " deleted!", "success");
                    },
                    function(httpResponse) {
                        $.notify("Failed to delete profile " + profile.id + "! (" + httpResponse.status + ": " + httpResponse.data.message + ")");
                    }
                );
            }
        });
    };
    $scope.copySelected = function() {
        var copyId = $scope.copyProfileId;
        $scope.copyProfileId = "";
        var profile = null;
        $.each($scope.profiles, function(index, val) {
            if(val.selected && profile == null)
                profile = val;
        });
        var profile = new Profile(profile);
        var oldId = profile.id;
        delete profile.id;
        delete profile.name;
        profile.$save({ token: $scope.token, id: copyId },
            function(saved, responseHeaders) {
                $.each($scope.profiles, function(index, val) {
                    if(val.id == saved.id)
                        $scope.profiles.splice(index, 1, saved);
                });
                $.each(OpenProfiles.openProfiles, function(index, val) {
                    if(val.id == saved.id)
                        OpenProfiles.openProfiles.splice(index, 1, saved);
                });
                $.notify("Profile " + oldId + " copied into profile " + profile.id + "!", "success");
            },
            function(httpResponse) {
                $.notify("Failed to copy profile! (" + httpResponse.status + ": " + httpResponse.data.message + ")");
            }
        );
    };
    $scope.addProfile = function() {
        var profile = new Profile({id: $scope.profileId, name: $scope.profileName, config: { 'profile>name': $scope.profileName } });
        $scope.profileId = "";
        $scope.profileName = "";
        profile.$insert({ token: $scope.token },
            function(saved, responseHeaders) {
                $scope.profiles.push(profile);
                $.notify("Profile " + profile.id + " created!", "success");
            },
            function(httpResponse) {
                $.notify("Failed to create profile! (" + httpResponse.status + ": " + httpResponse.data.message + ")");
            }
        );
    };
    $scope.deselect = function() {
        $.each($scope.profiles, function(index, value) {
            if(value.selected)
                value.selected = false;
        });
    };
    $scope.numSelected = function() {
        var selected = 0;
        $.each($scope.profiles, function(index, value) {
            if(value.selected)
                selected++;
        });
        return selected;
    };
    $scope.validId = function(id) {
        var regex = /\b[0-9]{4}\b/;
        return regex.test(id);
    };
    $scope.validName = function(name) {
        return typeof(name) === 'string' && name.length > 0;
    }
}]);

profileControllers.controller('ProfileCtrl', ['$scope', 'Profile', 'OpenProfiles', function($scope, Profile, OpenProfiles) {
    //$scope.openProfiles = OpenProfiles.get();
    //$scope.current = OpenProfiles.current;
    $scope.openService = OpenProfiles;
    $scope.token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1NWQ5N2QyOGI4ZTE2YWIwMjQyYmZjY2YiLCJ1c2VybmFtZSI6ImFwcCIsInBhc3N3b3JkIjoiJDJhJDEwJDZYendFL25hdHlQRGRqUnVXSk9nck9ERTJoOE5EdGhxeDVFcjlMdTIzbHFwdDNndkl1OXBHIiwic2FsdCI6IiQyYSQxMCQ2WHp3RS9uYXR5UERkalJ1V0pPZ3JPIiwiX192IjowfQ.zAHMFrB6wGkSclz6oBIZWWvFFN-9vZbJc6OEOFurQIg';
    $scope.selected = [];
    $scope.configArray = [{key: "test", value: "test"}];

    // watch the current profile so we can re-generate the array-version of the config (required to perform sorting by the key!)
    $scope.$watch('openService.current', function(newVal, oldVal, scope) {
        if(newVal)
            scope.configArray = scope.generateConfigArray();
    }, true);

    $scope.close = function() {
        // iterate open profiles and splice out current
        OpenProfiles.remove($scope.openService.current);
        $scope.openService.current = {};
        //$scope.current = null;
        $('a#profile-list-tab').click();
    };

    $scope.numSelected = function() {
        return $scope.selected.length;
    };
    $scope.isSelected = function(key) {
        return $scope.selected.indexOf(key) >= 0;
    };

    $scope.updateSelected = function(event, key) {
        // event should only ever be the checkbox event, so extract the clicked property to set the action
        var action = (event.target.checked ? "add" : "remove");
        // use action to add or remove key from selected list
        var existingIndex = $scope.selected.indexOf(key);
        if(action == "add" && existingIndex === -1)
            $scope.selected.push(key);
        else if(action == "remove" && existingIndex !== -1)
            $scope.selected.splice(existingIndex, 1);
    };

    $scope.copySelected = function() {
        // TODO
    };

    $scope.deleteSelected = function() {
        // delete each selected key from the current config
        angular.forEach($scope.selected, function (val, key) {
            delete $scope.openService.current.config[val];
        });
        // clear the selected rows
        $scope.selected.splice(0, $scope.selected.length);
        // save
        $scope.save();

    };

    $scope.update = function() {
        var key = $scope.key;
        var value = $scope.value;
        $scope.key = "";
        $scope.value = "";
        $scope.openService.current.config[key] = value;
        $scope.save();
    };

    $scope.save = function()
    {
        var profile = new Profile($scope.openService.current);
        profile.$save({ token: $scope.token, id: profile.id },
            function(saved, responseHeaders) {
                $.notify("Profile " + profile.id + " updated!", "success");
                $scope.openService.current = saved;
            },
            function(httpResponse) {
                $.notify("Failed to update profile! (" + httpResponse.status + ": " + httpResponse.data.message + ")");
            }
        );
        // regenerate config array
        $scope.configArray = $scope.generateConfigArray();
    };

    $scope.prefill = function(key, value) {
        $scope.key = key;
        $scope.value = value;
    };


    $scope.generateConfigArray = function()
    {
        var ary = [];
        angular.forEach($scope.openService.current.config, function (val, key) {
            ary.push({key: key, value: val});
        });
        return ary;
    }
}]);

profileControllers.controller('OpenProfilesCtrl', ['$scope', 'OpenProfiles', function($scope, OpenProfiles) {
    $scope.openProfiles = OpenProfiles.get();
    $scope.open = function(id) {
        OpenProfiles.setCurrent(id);
    };
}]);