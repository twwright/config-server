var profileApp = angular.module('profileApp', ['profileControllers', 'profileServices']);

// add toArray filter
profileApp.filter('toArray', function () {
    return function (obj, addKey) {
        if (!angular.isObject(obj)) return obj;
        if ( addKey === false ) {
            return Object.keys(obj).map(function(key) {
                return obj[key];
            });
        } else {
            return Object.keys(obj).map(function (key) {
                var value = obj[key];
                return angular.isObject(value) ?
                    Object.defineProperty(value, '$key', { enumerable: false, value: key}) :
                { $key: key, $value: value };
            });
        }
    };
});

// toKeyValObject filter
profileApp.filter('toKeyValObject', function () {
    var cache = [];
    return function(object) {
        cache.splice(0, cache.length);
        angular.forEach(object, function (value, key) {
            cache.push({key: key, value: value});
        });
        return cache;
    };
});