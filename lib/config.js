/**
 * Created by Tom on 12/07/2015.
 *
 * Contains definitions for various server configuration values
 *
 */

// database settings
module.exports.port = 3000;
module.exports.db = {
    host: "localhost",
    database: "appconfigs",
    user: "appconfigs",
    password: "mMqGZ07YUD7Z05m"
};

// JWT authentication settings
module.exports.jwt = {
    secret: "thesupersecret!",
    expiry: 60 // minutes
};