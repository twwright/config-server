var express = require('express');
var router = express.Router();

var bcrypt = require('bcryptjs');

// include the user database model
var UserModel = require('../models/user.js');

/** Retrieves a single user object matched by :userId */
router.get('/:userId', function(req, res, next) {
  UserModel.findOne({ _id: req.params.userId }, function(err, user) {
    if(err)
    {
      var error = {
        code: 500,
        error: 'db',
        message: 'Internal database error. Please contact administrator.',
        debug: JSON.stringify(err),
        jwt: req.jwtPayload
      };
      return next(error);
    }
    res.json(user || {} );
  });
});

/** Retrieves an array of user objects that match the search parameters given */
router.get('/', function(req, res, next) {
  // resolve query parameters from request
  var sortBy = req.query.sortBy || null;
  var sortOrder = req.query.sortOrder || null;
  var sortParams = {};
  if(sortBy != null)
    sortParams[sortBy] = sortOrder;
  var offset = req.query.offset || 0;
  var limit = req.query.limit || 10;
  UserModel.find().sort(sortParams).skip(offset).limit(limit).exec(function(err, users) {
    if(err)
    {
      var error = {
        code: 500,
        error: 'db',
        message: 'Internal database error. Please contact administrator.',
        debug: JSON.stringify(err),
        jwt: req.jwtPayload
      };
      return next(error);
    }
    res.json(users);
  });
});

/** Inserts the new user object included in request body */
router.post('/', function(req, res, next) {
  // create password salt and generate hash
  if(req.body.password)
  {
    req.body.salt = bcrypt.genSaltSync();
    req.body.password = bcrypt.hashSync(req.body.password, req.body.salt);
  }

  // create user from request body
  var user = new UserModel(req.body);

  // attempt to create new user object (validation applied)
  user.save(function(err, user) {
    if(err)
    {
      var error = {};
      if(err.name === 'ValidationError')
        error = {
          code: 400,
          error: 'bad-data',
          message: 'Submitted data or command failed internal validation! Ensure submitted data is correct according to the API constraints',
          debug: err.errors,
          jwt: req.jwtPayload
        };
      else
        error = {
          code: 500,
          error: 'db',
          message: 'Internal database error. Please contact administrator.',
          debug: JSON.stringify(err),
          jwt: req.jwtPayload
        };
      return next(error);
    }
    else
    {
      res.json(user);
    }
  });
});

/** Updates the given user with details included in request body */
router.put('/:userId', function(req, res, next) {
  // query for user
  UserModel.findOne({ _id: req.params.userId }, function(err, user) {
    // respond appropriately if an error occurred
    if(err)
    {
      var error = {
        code: 500,
        error: 'db',
        message: 'Internal database error. Please contact administrator.',
        debug: JSON.stringify(err),
        jwt: req.jwtPayload
      };
      return next(error);
    }
    // check if user exists
    if(!user)
    {
      var error = {
        code: 404,
        error: 'bad-data',
        message: 'Requested resource not found.',
        debug: 'Requested user resource with ID ' + req.params.userId + ' not found',
        jwt: req.jwtPayload
      };
      return next(error);
    }
    // exclude _id field from body if present (not mutable)
    delete req.body._id;
    // if updating password, create password salt and generate hash
    if(req.body.password)
    {
      req.body.salt = bcrypt.genSaltSync();
      req.body.password = bcrypt.hashSync(req.body.password, req.body.salt);
    }
    // merge fields from request body into user document
    for(var prop in req.body)
    {
      user[prop] = req.body[prop];
    }
    // attempt to save the user object
    user.save(function(err, user) {
      if(err)
      {
        var error = {};
        if(err.name === 'ValidationError')
          error = {
            code: 400,
            error: 'bad-data',
            message: 'Submitted data or command failed internal validation! Ensure submitted data is correct according to the API constraints',
            debug: err.errors,
            jwt: req.jwtPayload
          };
        else
          error = {
            code: 500,
            error: 'db',
            message: 'Internal database error. Please contact administrator.',
            debug: JSON.stringify(err),
            jwt: req.jwtPayload
          };
        return next(error);
      }
      res.json(user);
    });
  });
});

/** Deletes a single user object matched by :userId */
router.delete('/:userId', function(req, res, next) {
  UserModel.findOneAndRemove({ _id: req.params.userId }, function(err, user) {
    // respond appropriately if error occurred
    if(err)
    {
      var error = {
        code: 500,
        error: 'db',
        message: 'Internal database error. Please contact administrator.',
        debug: JSON.stringify(err),
        jwt: req.jwtPayload
      };
      return next(error);
    }
    // return removed user document or an empty object
    res.json(user || {});
  });
});

module.exports = router;
