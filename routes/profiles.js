var express = require('express');
var router = express.Router();

// include the user database model
var ProfileModel = require('../models/profile.js');

/** Retrieve all profiles! **/
router.get('/', function(req, res, next) {
    // retrieve profiles from database
    ProfileModel.find(function(err, profiles) {
        if(err)
        {
            var error = {
                code: 500,
                error: 'db',
                message: 'Internal database error. Please contact administrator.',
                debug: JSON.stringify(err),
                jwt: req.jwtPayload
            };
            return next(error);
        }
        res.json(profiles);
    });
});

/** Retrieves a single profile object matched by :profileId */ // TODO fix up sid and digit IDs
router.get('/:profileId', function(req, res, next) {
    ProfileModel.findOne({ id: req.params.profileId }, function(err, profile) {
        if(err)
        {
            var error = {
                code: 500,
                error: 'db',
                message: 'Internal database error. Please contact administrator.',
                debug: JSON.stringify(err)
                //jwt: req.jwtPayload
            };
            return next(error);
        }
        res.json(profile || {} );
    });
});

/** Retrieves a single profile object matched by :profileId, using friendly ID*/
router.get('/:profileId/id', function(req, res, next) {
    ProfileModel.findOne({ id: req.params.profileId }, function(err, profile) {
        if(err)
        {
            var error = {
                code: 500,
                error: 'db',
                message: 'Internal database error. Please contact administrator.',
                debug: JSON.stringify(err)
                //jwt: req.jwtPayload
            };
            return next(error);
        }
        res.json(profile || {} );
    });
});

/** Inserts the profile document included in the request body as a new document */
router.post('/', function(req, res, next) {
    // create timestamps and delete any possible id
    delete req.body._id;
    req.body.created = Date.now();
    req.body.modified = Date.now();

    // create user from request body
    var profile = new ProfileModel(req.body);

    // attempt to create new profile object (validation applied)
    profile.save(function(err, profile) {
        if(err)
        {
            var error = {};
            if(err.name === 'ValidationError')
                error = {
                    code: 400,
                    error: 'bad-data',
                    message: 'Submitted data or command failed internal validation! Ensure submitted data is correct according to the API constraints',
                    debug: err.errors,
                    jwt: req.jwtPayload
                };
            else
                error = {
                    code: 500,
                    error: 'db',
                    message: 'Internal database error. Please contact administrator.',
                    debug: JSON.stringify(err),
                    jwt: req.jwtPayload
                };
            return next(error);
        }
        else
        {
            res.json(profile);
        }
    });
});

/** Updates the given profile with details included in request body */
router.put('/:profileId', function(req, res, next) {
    // query for profile
    ProfileModel.findOne({ id: req.params.profileId }, function(err, profile) {
        // respond appropriately if an error occurred
        if(err)
        {
            var error = {
                code: 500,
                error: 'db',
                message: 'Internal database error. Please contact administrator.',
                debug: JSON.stringify(err),
                jwt: req.jwtPayload
            };
            return next(error);
        }
        // check if user exists
        if(!profile)
        {
            var error = {
                code: 404,
                error: 'bad-data',
                message: 'Requested resource not found.',
                debug: 'Requested profile resource with ID ' + req.params.profileId + ' not found',
                jwt: req.jwtPayload
            };
            return next(error);
        }

        // exclude _id field from body if present (not mutable)
        delete req.body._id;
        delete req.body.created;
        req.body.modified = Date.now();

        // merge fields from request body into profile document
        for(var prop in req.body)
        {
            if(req.body.hasOwnProperty(prop))
                profile[prop] = req.body[prop];
        }

        // attempt to save the user object
        profile.save(function(err, profile) {
            if(err)
            {
                var error = {};
                if(err.name === 'ValidationError')
                    error = {
                        code: 400,
                        error: 'bad-data',
                        message: 'Submitted data or command failed internal validation! Ensure submitted data is correct according to the API constraints',
                        debug: err.errors,
                        jwt: req.jwtPayload
                    };
                else
                    error = {
                        code: 500,
                        error: 'db',
                        message: 'Internal database error. Please contact administrator.',
                        debug: JSON.stringify(err),
                        jwt: req.jwtPayload
                    };
                return next(error);
            }
            res.json(profile);
        });
    });
});

/** Deletes a single profile object matches by :profileId */
router.delete('/:profileId', function(req, res, next) {
    ProfileModel.findOneAndRemove({ id: req.params.profileId }, function(err, profileId) {
        // respond appropriately if error occurred
        if(err)
        {
            var error = {
                code: 500,
                error: 'db',
                message: 'Internal database error. Please contact administrator.',
                debug: JSON.stringify(err),
                jwt: req.jwtPayload
            };
            return next(error);
        }
        // return removed user document or an empty object
        res.json(profileId || {});
    });
});

module.exports = router;